from random import shuffle

def makeDeck(deckAmt):
    deck = []
    for i in range(9):
        for j in range(4*deckAmt):
            deck.append(i+1)
    for i in range(16*deckAmt):
        deck.append(0)
    deck = list(map(str, deck))
    ogDeck = deck[:]
    shuffle(deck)
    return deck, ogDeck