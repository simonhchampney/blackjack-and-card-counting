import deck as dk
import scoring
from random import shuffle
def deal(deck):
    player1 = []
    player2 = []
    dealer = []
    deck1 = deck[:]
    
    for i in range(2):
        player1.append(deck1[-1])
        deck1.pop()
        player2.append(deck1[-1])
        deck1.pop()
        dealer.append(deck1[-1])
        deck1.pop()
    
    deck = deck1[:]
    
    return player1, player2, dealer, deck

def main():
    chips = 500
    deck, ogDeck = dk.makeDeck(8)
    while(True):
        if(chips<=0):
            print("You ran out of chips")
            break
        print("You have", chips, "chips")
        print("How much you betti'n?")
        if(len(deck)<=len(ogDeck)/2):
            deck = ogDeck[:]
            shuffle(deck)
            print("Dealer reshuffled")
        
        while(True):
            bet = float(input(""))
            if(bet<=chips and bet>0):
                chips-=bet
                break
            elif(bet<=0):
                print("Must be positive")
            else:
                print("You don't have enough")
        
        player1, player2, dealer, deck = deal(deck)
        
        print("\033cYou have:", str(player1))
        print("Other player has:", str(player2))
        
        score = scoring.getHandScore(player1)[0] # player1 plays
        if(score == 21):
            print("Blackjack!")
            chips+=bet*1.5
        else:
            while(True):
                score = scoring.getHandScore(player1)[0]
                if(score>21):
                    print("You bust!")
                    break
                print("You have", score, "\nDealer has", dealer[0])
                print("Hit or Stand?")
                op = input("")
                hit = ("H" == op or "h" == op or "hit" == op or "Hit" == op)
                
                if(hit):
                    a = deck[-1]
                    player1.append(a)
                    deck.pop()
                    print("\033cYou hit:", str(player1[-1]))
                else:
                    break
        # player1 is done
        
        while(True): # other player plays
            score = scoring.getHandScore(player2)[0]
            scoreD = scoring.getHandScore([dealer[0]])[0]
            
            if(score>21):
                print("Other player busts")
                break
            hit = False
            if(score<=11):
                hit == True
            elif(score == 12 and (scoreD < 3 or scoreD > 7)):
                hit == True
            elif(score>12 and score<17 and scoreD > 6):
                hit == True
            
            if(hit):
                player2.append(deck[-1])
                deck.pop()
                print("Other player hits:", str(player2[-1]))
            else:
                print("Other player stands")
                break
        # other player is done
        
        print("Dealer had:", dealer) # dealer goes
        while(True):
            score = scoring.getHandScore(dealer)[0]
            if(score > 21):
                print("Dealer busts!")
                break
            hit = score<17
            
            if(hit):
                dealer.append(deck[-1])
                deck.pop()
                print("Dealer hits:", str(dealer[-1]))
            else:
                print("Dealer stands")
                break
        #dealer is done
        
        p1S = scoring.getHandScore(player1)[0]
        dS = scoring.getHandScore(dealer)[0]
        if(p1S>21):
            print("You busted at", p1S)
            continue
        elif(dS>21):
            print("The dealer busted at", dS)
            chips+=bet*2
        elif(p1S == dS):
            print("You and the dealer had", p1S)
            chips+=bet
        elif(p1S < dS):
            print("Dealer had", dS, "\nYou only had", p1S)
        else:
            print("You had", p1S, "\nDealer only had", dS)
            chips+=bet*2