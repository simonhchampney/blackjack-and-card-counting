def getHandScore(hand):
    score = 0
    soft = False
    for card in hand:
        if(card[0] == "0"):
            score+=10
        else:
            score+=int(card[0])
    if("1" in hand):
        if(score<=11):
            score+=10
            soft = True
    return score, soft



def getDeckScore(deck):
    amt = 0
    for card1 in deck:
        if(card1[0] == "0"):
            amt -= 3
        elif(card1[0] == "5"):
            amt += 3
        elif(card1[0] == "9"):
            amt -= 1
        else:
            for i in range(2, 8):
                if(str(i) == card1[0]):
                    amt += 2
                    break
    return -amt