from misc import input_no
import deck as dk
import scoring

def calcChance(player_score, dealer_upcard, deck):
    amtUnder = 0
    for card in deck:
        num = 0
        if card == "1":
            num = 1
        elif card == "0":
            num = 10
        else:
            num = int(card)
        
        # Calculate the maximum possible player score after taking an additional card
        max_player_score = player_score + num
        if "1" in deck:
            if max_player_score <= 11:
                max_player_score += 10
        
        if max_player_score <= 21:
            amtUnder += 1
        
        # Consider the dealer's upcard
        dealer_num = 0
        if dealer_upcard == "1":
            dealer_num = 1
        elif dealer_upcard == "0":
            dealer_num = 10
        else:
            dealer_num = int(dealer_upcard)
        
        # Calculate the maximum possible dealer score after taking an additional card
        max_dealer_score = dealer_num + scoring.getHandScore([card])[0]
        if "1" in deck:
            if max_dealer_score <= 11:
                max_dealer_score += 10
        
        # Check if the dealer is likely to go bust after taking an additional card
        if max_dealer_score > 21:
            amtUnder += 1
    
    return (amtUnder / len(deck)) * 100

def suggest(playerSc, dealerSc, soft, deck):
    if(dealerSc == 0):
        dealerSc=10
    chanceLess = calcChance(playerSc, dealerSc, deck)
    if(soft):
        if(playerSc<18):
            return "hit"
        elif(playerSc == 18):
            if(dealerSc<9):
                if(chanceLess>=60):
                    return "hit"
                else:
                    return "stay"
            else:
                return "hit"
        else:
            if(chanceLess>=70):
                return "hit"
            else:
                return "stay"
    else:
        if(playerSc<=11):
            return "hit"
        elif(playerSc == 12):
            if(dealerSc < 4 or dealerSc > 6):
                if(chanceLess>=40):
                    return "hit"
                else:
                    return "stay"
            else:
                if(chanceLess>=60):
                    return "hit"
                else:
                    return "stay"
        elif(playerSc > 12 and playerSc < 17):
            if(playerSc > 6):
                if(chanceLess>=50):
                    return "hit"
                else:
                    return "stay"
            else:
                if(chanceLess>=70):
                    return "hit"
                else:
                    return "stay"
        else:
            if(chanceLess>=80):
                return "hit"
            else:
                return "stay"

# Set up players hand
def getPlayersHand(deck):
    print("What is your first card? (0 for 10)")
    card = input_no()
    if(card.upper() in deck):
        deck.remove(card.upper())
    else:
        print("Error")
    hand = [card]
    print("What is your second card? (0 for 10)")
    card = input_no()
    if(card.upper() in deck):
        deck.remove(card.upper())
    else:
        print("Error")
    hand.append(card)
    return hand, deck

# Other players?
def otherCards(deck):
    while(True):
        print("Any other cards (0 for faces, e for done)")
        card = input_no()
        if(card=="e"):
            break
        deck.remove(card.upper())
    return deck

def getDealers(deck):
    print("\033cWhat score is the dealer so far? (0 for faces)")
    deal = input_no()
    deck.remove(deal)
    return int(deal), deck

def getMoreCards(deck):
    print("What's the card you got from hitting?")
    card = input_no()
    deck.remove(card)
    return card, deck

def main():
    print("How many decks?")
    deckAmt = int(input_no())
    deck, ogDeck = dk.makeDeck(deckAmt)
    print("\033cWhat is the minimum bet?")
    minimum = float(input(""))
    while(True):
        # Get bet suggestion
        print("You may want to bet:", minimum*(9+scoring.getDeckScore(deck)))

        hand, deck = getPlayersHand(deck)
        deck = otherCards(deck)
        deal, deck = getDealers(deck)

        while(True):
            # Get suggestion
            handSc = scoring.getHandScore(hand)
            do = suggest(handSc[0], deal, handSc[1], deck)
            print("You should ", do)

            if(do=="stay"):
                break
            card, deck = getMoreCards(deck)
            hand.append(card)
        deck = otherCards(deck)
        
        print("Did they reshuffle? (y/n)")
        if(input_no()=="y"):
            deck=shuffle(ogDeck)
        elif(len(deck) == 0):
            print("They still didn't reshuffle???")
            break






        
                