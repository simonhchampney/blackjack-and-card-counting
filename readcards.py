import cv2
import numpy as np
import os
import pytesseract
import pyautogui
import re

# Set up Tesseract OCR (You need to install Tesseract OCR and pytesseract)
pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'  # Linux/macOS

# Load card templates
template_folder = "templates"
template_files = os.listdir(template_folder)
card_templates = {}
for template_file in template_files:
    template_path = os.path.join(template_folder, template_file)
    symbol = os.path.splitext(template_file)[0].upper()
    card_templates[symbol] = cv2.imread(template_path, 0)

def preprocess_image(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    return blurred

def find_playing_cards(image):
    # Initialize list to store detected cards
    detected_cards = []

    # Find card symbols using OCR
    text = pytesseract.image_to_string(image, lang='eng')
    detected_symbols = re.findall(r'\b[A23456789TJQK]{1,2}\b', text)

    # Find card symbols using template matching
    for symbol, template in card_templates.items():
        res = cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)
        loc = np.where(res >= 0.7)
        for pt in zip(*loc[::-1]):
            detected_symbols.append(symbol)

    # Remove duplicates
    detected_symbols = list(set(detected_symbols))

    return detected_symbols

def main():
    try:
        while True:
            # Capture the screen
            screenshot = pyautogui.screenshot()
            image = np.array(screenshot)
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

            # Preprocess the image
            preprocessed_image = preprocess_image(image)

            # Find playing cards in the image
            cards = find_playing_cards(preprocessed_image)

            if cards:
                print("Detected playing cards:", cards)

            # Delay before taking the next screenshot (adjust this as needed)
            pyautogui.sleep(2)
    except KeyboardInterrupt:
        print("Program terminated.")

if __name__ == "__main__":
    main()
