## Name
Blackjack and Card Counting

## Description
This software allows users to count the amount of cards left in a deck and allows one to get the count of a blackjack deck. This program works for multiple decks and also allows for testing of the reliability of the counting of the cards.

## Installation
It is suggested that rather than installing the program(s) you instead just clone the repository and run it in your visual studio code. 

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
If help is needed it is suggested that you go to Chatgpt.

## Project status
Currently work is stil being done, most of it all stems from a previous version but now I've got this one for easier usage.
